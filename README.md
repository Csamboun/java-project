# Readme

#### Auteurs
Jubault Gabin - *Etudiant 1ere année ENSG* -  
Oxombre Victor - *Etudiant 1ere année ENSG* -  
Samboun Christophe - *Etudiant 1ere année ENSG* -

## Prérequis
- Afin de pouvoir exécuter notre application, il faut avoir Java d’installé sur sa machine pour réussir à le lire. Si vous ne l’avez pas déjà, vous pouvez le télécharger sur le site de [Java](https://www.java.com/fr/download/).  
- L’application utilise une base de données. Il faut donc avoir un système de gestion de base de données (SGBD) (ex. : PostgreSQL) et un client afin d’y accéder facilement (ex. : pgAdmin).    
- De nombreuses requêtes HTTP sont envoyées, principalement vers Google Maps. L’application a donc besoin d’une connexion internet pour fonctionner.  

## Comment l'utiliser

### Ouverture de l'application
Extraire le fichier zip *ProjetJava*  
Double-clic sur le fichier *ProjetJava.jar*

### Utilisation
A l'ouverture du programme, rentrez le nom d'utilisateur et le mot de passe suivants:  

#### Login Admin
username: Admin  password: admin  

#### Login Livreur
username: Livreur  password: Livreur   

Maintenant que vous êtes connectés, une fenêtre  livreur / administration s'affiche.  
Vous pouvez maintenant accéder à la liste de commandes faites par des clients prédéfinis et au stock de l'entrepôt.

#### Partie service administratif
Vous avez la possibilité de valider les commandes et d'éditer les factures. Pour ce faire, sélectionnez la commande que vous voulez traiter et dans la fenêtre en dessous de la liste de commande, vous pourrez "*Remplir les stocks*" si vous remarquez que le nombre de bien disponible est inférieur à la demande du client, "*Valider une commander*" si le stock de l'entrepôt permet la livraison et "Editer une facture" afin de communiquer au client la validation de la commande.

#### Partie service livraison
Vous avez la possibilité de choisir des commandes pour effectuer une tournée. Si le poids et le volume maximal possible pour une tournée ont été respecté, alors le statut des commandes sélectionnées passeront à "*En cours*". Ensuite, vous pourrez afficher le trajet de la livraison en cliquant sur "*afficher carte*", effectuer un "*Retour entrepôt*" si la commande n'a pas pu être livrée et valider la commande dans le cas contraire.

### Language de programmation
Java